<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Gato</title>
</head>
<body>
	<div class="curriculo">
		<div class="contato">

			<h2>Formulario de curriculum</h2>

			<form class="form" method="POST" action="upload.php" enctype="multipart/form-data">

				<div class="pessoal">
					<h3>Pessoal</h3>
					<input type="text" name="nome" class="field" placeholder="Nome">
					<input type="text" name="cpf" class="field" placeholder="CPF">
					<input type="text" name="cep" class="field" placeholder="CEP">
					<input type="text" name="rua" class="field" placeholder="Rua">
					<input type="text" name="bairro" class="field" placeholder="Bairro">
					<input type="text" name="cidade" class="field" placeholder="Cidade">
					<input type="text" name="email" class="field" placeholder="email">
					<input type="text" name="telefone" class="field" placeholder="Telefone">
				</div>

				<div class="emprego">
					<h3>Empresas</h3>
					<input type="text" name="nomeEmpresa" class="field" placeholder="Nome">
					<input type="text" name="cargoEmpresa" class="field" placeholder="Cargo">
					<input type="text" name="cidadeEmpresa" class="field" placeholder="Cidade">
					<input type="text" name="telefoneEmpresa" class="field" placeholder="Telefone">
					<textarea name="funcoesEmpresa" class="field"  placeholder="Nós diga quais funções voce empenhava na empresa"></textarea>
				</div>

				<div class="sobreVoce">
					<textarea name="aboutYou" class="field"  placeholder="Digite algo sobre voce"></textarea>
				</div>

				<div>
					<label>Arquivo:</label><br/>
					<input type="file" name="arquivo" /><br/><br/>
				</div>
				<input class="field" type="submit" value="Enviar">

			</form>
		</div>

	</div>
</body>
</html>
